import Image from 'next/image';
import './s-purpose.scss'

const purpose = [
  `Формирование <span> единого цифрового информационного пространства</span>, позволяющего создавать автоматизированные рабочие места врачей;`,
  `Организовывать работу отдела медицинской статистики;`,
  `Создавать и обрабатывать базы данных, вести электронные истории болезней;`,
  `Объединение в единое целое всех лечебных, диагностических процессов.`,
]

const SPurpose = () => {
  return (
    <section className="s-purpose">
      <div className="l-default s-purpose__container">
        <div className="s-purpose__title">
          <h2 className="title--h2">цель создания</h2>
          <p className="s-purpose__title-prefix title--prefix">объединенной экосистемы EVA</p>
        </div>
        <div className="s-purpose__content">
          <div className="s-purpose__img">
            {/* <Image
              src=''
              fill
            /> */}
          </div>
          <div className="s-purpose__items">
            {purpose.map((item, index) => {
              return(
                <div key={index} className="s-purpose__item">
                <div className="s-purpose__item-container">
                  <div className="s-purpose__item-arrow">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect width="30" height="30" rx="15" fill="white"/>
                      <path d="M20.7803 15.5303C21.0732 15.2374 21.0732 14.7626 20.7803 14.4697L16.0074 9.6967C15.7145 9.40381 15.2396 9.40381 14.9467 9.6967C14.6538 9.98959 14.6538 10.4645 14.9467 10.7574L19.1893 15L14.9467 19.2426C14.6538 19.5355 14.6538 20.0104 14.9467 20.3033C15.2396 20.5962 15.7145 20.5962 16.0074 20.3033L20.7803 15.5303ZM9 15.75L20.25 15.75L20.25 14.25L9 14.25L9 15.75Z" fill="#0068FF"/>
                    </svg>
                  </div>
                  <div className="s-purpose__item-text"dangerouslySetInnerHTML={{ __html: item }} />
                </div>
              </div>
              )})}
          </div>
        </div>
      </div>
    </section>
  )
} 
export default SPurpose;