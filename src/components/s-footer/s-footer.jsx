import React from 'react';
import Image from 'next/image';
import  './s-footer.scss'
const Footer = () => {
  return (
    <footer className="s-footer">
      <div className="l-default">
        <div className="s-footer__container">
          <div className="s-footer__row">
            <div id='contacts' className="s-footer__row-item">
              <div className="s-footer__row-title ligth">EVA Bioengineering</div>
              <div className="s-footer__row-text">
                Регистрационный номер в Реестре операторов персональных данных: 77-23-151812
                основание: приказа №275 от 15.08.2023 г.
              </div>
              <div className="s-footer__row-title">Адрес</div>
              <div className="s-footer__row-text">РФ, г. Москва, улица Фридриха Энгельса, д.46с7, оф. 46</div>
            </div>
            <div className="s-footer__row-item">
              <div className="s-footer__row-title">Контакты для обратной связи:</div>
              <div className="s-footer__row-text ligth">
                Юридический отдел <br/>
                эл. почта: <a href="mailto:oantipina@eva-bio.ru" className='link'>oantipina@eva-bio.ru</a>
              </div>
                <div className="s-footer__row-title">Партнерам</div>
                <div className="s-footer__row-text">
                  <a href="mailto:partner@eva-bio.ru" className='link'>partner@eva-bio.ru</a>
                </div>
            </div>
          </div>
          <div className="s-footer__column">
            <div className="s-footer__column-item">
              © {new Date().getFullYear()} EVA Bioengineering
            </div>
            <a href='/privacy-policy' className="s-footer__column-item link">Политика конфиденциальности</a>
            <a href='/terms-of-use' className="s-footer__column-item link">Пользовательское соглашение</a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;