import './s-integration.scss'

const integrations = [
  'ГБУЗ МО «Домодедовская центральная городская больница»',
  'ГБУЗ МО МОНИКИ им. М.Ф. Владимирского',
  'ГБУЗ МО «Солнечногорская Областная больница»',
  'РНИМУ им. Н.И. Пирогова, НИИ урологии и интервенционной радиологии им. Н. А. Лопаткина',
  'Подольская областная клиническая больница',
  'ГБУЗ МО «Ногинская ЦРБ'
]

const SIntegration = () =>{
  return (
    <section className="s-integration">
      <div className="s-integration__container">
        <div className="s-integration__top l-default">
          <div className="s-integration__title ">
            <h2 className="title--h2 ">
              ИТОГИ ИНТЕРГРАЦИИ
            </h2>
            <p className="s-integration__title-prefix title--prefix">на 2023 год</p>
          </div>
        </div>
        <div className="s-integration__bottom l-default">
          <div className="s-integration__content">
            <div className="s-integration__items">
              { integrations.map((item, index)=>{
                return (
                  <div key={index} className="s-integration__item">
                    <div className="s-integration__item-container">
                      <div className="s-integration__item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                          <path d="M7.6 12.6L4.1 9.1C3.7134 8.7134 3.0866 8.7134 2.7 9.1C2.3134 9.4866 2.3134 10.1134 2.7 10.5L7.6 15.4L18.9 4.1C19.2866 3.7134 19.2866 3.0866 18.9 2.7C18.5134 2.3134 17.8866 2.3134 17.5 2.7L7.6 12.6Z" fill="#0068FF"/>
                          <circle opacity="0.2" cx="8" cy="11" r="8" fill="#0068FF"/>
                        </svg>
                      </div>
                      <div className="s-integration__item-text">{item}</div>
                    </div>
                  </div>
                )})
              }
            </div>
            <div className="s-integration__content-prefix">
              <div className="s-integration__prefix-number">+28</div>
              <div className="s-integration__prefix-text">других медицинских организаций МО</div>
            </div>
          </div>
        </div>

      </div>
    </section>
  )
}

export default SIntegration