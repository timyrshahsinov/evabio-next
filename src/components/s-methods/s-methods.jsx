"use client"
import './s-methods.scss'
import React, { useState } from 'react';

const initialMethods = [
  {
    isShow:true,
    id:'CDSS',
    title:'Система поддержки принятия клинических решений',
    description:[
      `Это информационная технология в области здравоохранения, которая предоставляет врачам, персонал, пациентам и другим лицам знания и информацию, касающуюся конкретного человека, для содействия здоровью и медицинскому обслуживанию. CDSS включает в себя множество инструментов для улучшения процесса принятия решений в клиническом процессе.`,
      `Эти инструменты включают в себя компьютеризированные оповещения и напоминания поставщикам медицинской помощи и пациентам, клинические рекомендации, наборы заказов для конкретных состояний, отчеты и резюме с конкретными данными о пациентах, шаблоны документации, диагностическую поддержку и контекстуально релевантную справочную информацию, среди прочих инструментов.`
    ],
    prefix:'Основная цель современных CDSS - помогать врачам на месте оказания медицинской помощи. Это означает, что врачи взаимодействуют с CDSS, чтобы помочь проанализировать и поставить диагноз на основе данных пациента по различным заболеваниям.'
  },
  {
    isShow:false,
    id:'EHR',
    title:'Объединённые электронные медицинские карты пациентов',
    description:['Это способ сбора и использования данных в режиме реального времени для оказания высококачественной помощи пациентам, гарантируя эффективность и рациональное использование времени и ресурсов.'],
    prefix:''
  },
  {
    isShow:false,
    id:'GBDT',
    title:'Метод градиентного бустинга',
    description:['Это продвинутый алгоритм машинного обучения для решения задач классификации и регрессии. Он строит предсказание в виде ансамбля слабых предсказывающих моделей, которыми в основном являются деревья решений. Из нескольких слабых моделей в итоге мы собираем одну, но уже эффективную.'],
    prefix:'В ходе обучения каждый базовый алгоритм строится независимо от остальных. Бустинг, в свою очередь, воплощает идею последовательного построения линейной комбинации алгоритмов. Каждый следующий алгоритм старается уменьшить ошибку текущего ансамбля.'
  },
  {
    isShow:false,
    id:'CNN',
    title:'Сверточная нейронная сеть',
    description:['Это специальная архитектура нейронных сетей, нацеленная на эффективное распознавание изображений, предобученных на большом наборе данных.'],
    prefix:''
  },
]

const SMethods = () => {
  // Использование useState для создания изменяемого состояния
  const [methods, setMethods] = useState(initialMethods);

  // Функция для изменения isShow
  const toggleIsShow = (index) => {
    const newMethods = [...methods];
    newMethods[index].isShow = !newMethods[index].isShow; // Переключение значения
    setMethods(newMethods);
  };

  return (
    <section className="s-methods">
      <div className="l-default s-methods__container">
        <div className="s-methods__title">
          <p className="s-methods__title-subtitle title--prefix">Принципы работы алгоритмов</p>
          <h2 className="title--h2">
            описание методов <span>взаимодействия систем</span>
          </h2>
          <p className="s-methods__title-prefix title--prefix">внутри экосистемы EVA</p>
        </div>
        <div className="s-methods__content">
          <div className="s-methods__items">
            {methods.map((item, index)=>{
              return(
                <>
                  <div key={index} className="s-methods__item">
                    <div className="s-methods__item-container">
                      <div className="s-methods__item-left">
                        <div className="s-methods__item-name">{item.id}</div>
                        <div className="s-methods__item-title">{item.title}</div>
                      </div>
                      <div className="s-methods__item-btn" onClick={() => toggleIsShow(index)}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70" fill="none">
                          <g clipPath="url(#clip0_2097_5800)">
                            {!item.isShow && <path d="M37.1914 37.8098C37.7248 37.6668 38.0414 37.1185 37.8985 36.585L35.5691 27.8917C35.4262 27.3582 34.8778 27.0416 34.3444 27.1846C33.8109 27.3275 33.4943 27.8759 33.6373 28.4093L35.7078 36.1367L27.9804 38.2073C27.4469 38.3502 27.1304 38.8986 27.2733 39.432C27.4162 39.9655 27.9646 40.2821 28.498 40.1391L37.1914 37.8098ZM19.2861 27.8104L36.4326 37.7099L37.4326 35.9778L20.2861 26.0783L19.2861 27.8104Z" fill="#0068FF"/>}
                            {
                              item.isShow && <path d="M21.3594 23.894C20.8071 23.894 20.3594 24.3418 20.3594 24.894L20.3594 33.894C20.3594 34.4463 20.8071 34.894 21.3594 34.894C21.9117 34.894 22.3594 34.4463 22.3594 33.894L22.3594 25.894L30.3594 25.894C30.9117 25.894 31.3594 25.4463 31.3594 24.894C31.3594 24.3418 30.9117 23.894 30.3594 23.894L21.3594 23.894ZM36.0665 38.1869L22.0665 24.1869L20.6523 25.6012L34.6523 39.6011L36.0665 38.1869Z" fill="#0068FF"/>
                            }
                            <circle cx="28.006" cy="32.5062" r="23.5" transform="rotate(75 28.006 32.5062)" stroke="#0068FF" strokeWidth="2"/>
                            <path d="M46.0117 2.50615C49.9922 4.80428 53.4811 7.86393 56.2791 11.5104C59.0771 15.1568 61.1295 19.3187 62.3191 23.7584C63.5087 28.198 63.8122 32.8285 63.2123 37.3855C62.6124 41.9424 61.1207 46.3366 58.8226 50.317C56.5245 54.2975 53.4648 57.7864 49.8184 60.5844C46.1719 63.3824 42.01 65.4348 37.5704 66.6244C33.1307 67.814 28.5002 68.1175 23.9433 67.5176C19.3864 66.9177 14.9922 65.4261 11.0117 63.1279" stroke="#0068FF" strokeWidth="2"/>
                          </g>
                          <defs>
                            <clipPath id="clip0_2097_5800">
                              <rect width="70" height="70" fill="white"/>
                            </clipPath>
                          </defs>
                        </svg>
                      </div>

                    </div>
                    {item.isShow && (
                      <div className="s-methods__item-text">
                        {item.description.map((description, indexex) =>{
                          return(
                            <p key={indexex}>{description}</p>
                          )
                        })}
                      </div>
                    )}
                    {item.isShow && item.prefix!== '' && ( // Используйте здесь item.isShow, если есть такое свойство
                      <div className="s-methods__item-prefix">{item.prefix}</div>
                    )}
                  </div>
                  {index === 1 && 
                    <div className="s-methods__item block">
                      <div className="s-methods__block">
                        <div className="s-methods__block-icon">
                          <svg width="60" height="61" viewBox="0 0 60 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect y="0.754395" width="60" height="60" rx="30" fill="white"/>
                            <rect x="14.598" y="26.0014" width="13" height="23.0909" rx="6.5" transform="rotate(-36.5152 14.598 26.0014)" fill="#CCE1FF" stroke="#0068FF" strokeWidth="2"/>
                            <rect x="35.2191" y="18.1595" width="13" height="23.0909" rx="6.5" transform="rotate(38.4848 35.2191 18.1595)" stroke="#0068FF" strokeWidth="2"/>
                          </svg>
                        </div>
                        <div className="s-methods__block-text">
                          Будущее высокотехнологичной медицины <span>Экосистема EVA = CDSS + EHR + GBDT + CNN</span>
                        </div>
                      </div>
                    </div>
                  }
                </>
              )})}
          </div>
        </div>
      </div>
    </section>
  )
}

export default SMethods