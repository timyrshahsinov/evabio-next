'use client'

import './s-header.scss'
import React,{useState, useEffect,useRef} from 'react';
import { useRouter } from 'next/navigation';
import Image from 'next/image';
const Header = () => {
  const links = [
    {
      href: '/#purprose',
      name: 'Цели'
    },
    {
      href: '/#metods',
      name: 'Методы'
    },
    {
      href: '/#integration',
      name: 'Интеграции'
    },
    {
      href: '/#contacts',
      name: 'Контакты'
    },
  ];

  const [windowWidth, setWindowWidth] = useState(undefined);
  const [logoSrc, setLogoSrc] = useState("/s-header/logo.svg");
  const [isActive, setIsActive] = useState(false);
  const router = useRouter();
  const headerRef = useRef();

  const showItems = () => {
    setIsActive(!isActive);
    if (!isActive) {
      document.body.style.overflowY = 'hidden';
      document.documentElement.style.overflowY = 'hidden';
    } else {
      document.body.style.overflowY = 'initial';
      document.documentElement.style.overflowY = 'initial';
    } // инвертируем состояние
  };
  
  const scrollToSection = (href) => {
    const id = href.split('#')[1];
    if (id) {
      // Проверяем, находимся ли мы на главной странице
      if (window.location.pathname !== '/') {
        // Если нет, то перенаправляем на главную страницу и добавляем хэш
        router.push('/#' + id);
      } else {
        // Если да, то прокручиваем к секции
        const element = document.getElementById(id);
        if (element) {
          const headerHeight = headerRef.current.clientHeight; // Получаем высоту хедера
          const position = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
          window.scrollTo({ top: position, behavior: 'smooth' });
        }
      }
    }
  };

  useEffect(() => {
    // Обновление ширины окна при монтировании компонента
    function handleResize() {
      setWindowWidth(window.innerWidth);
    }

    window.addEventListener('resize', handleResize);
    
    // Вызов handleResize сразу же для инициализации windowWidth
    handleResize();

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    // Обновление пути к изображению при изменении ширины окна
    const logoPath = windowWidth <= 767 ? "/s-header/logo-mobile.svg" : "/s-header/logo.svg";
    setLogoSrc(logoPath);
  }, [windowWidth]);

  return (
    <header ref={headerRef} className='s-header'>
      <div className='l-default'>
        <div className="s-header__container">
          <div className='s-header__logo'>
            <a href="/" className='logo'>
              <Image
                src={logoSrc}
                alt="Логотип"
                fill
              />
            </a>
          </div>
          <div className='s-header__items'>
            {links.map((link) => (
              <div key={link.href} className='s-header__item'>
                <a className='text-m link' href={link.href} onClick={(e) => {
              e.preventDefault();
              scrollToSection(link.href);
            }}>{link.name}</a>
              </div>
            ))}           
            <div className='s-header__item'>
              <a href="#" className='s-header__link link'>Вход</a>
            </div>
          </div>            
          <button className={isActive ? 's-header__burger isActive':'s-header__burger'} onClick={showItems}>
            <span></span>
            <span></span>
            <span></span>
          </button>
          <div className={isActive ? 'isActive s-header__mobile' : 's-header__mobile'}>
            <div className="s-header__mobile-items">
              {links.map((link) => (
                <div key={link.href} className='s-header__mobile-item'>
                  <a className='text-m link' onClick={(e) => {
                    showItems()
              e.preventDefault();
              scrollToSection(link.href);
            }} href={link.href}>{link.name}</a>
                </div>
              ))} 
            </div>
            <div className="s-header__mobile-contacts">
              <p className="s-header__contacts-title">
                Контакты для обратной связи:
              </p>
              <p className="s-header__contacts-content">
                Юридический отдел<br/> эл. почта: <a href="mailto:oantipina@eva-bio.ru">oantipina@eva-bio.ru</a>
              </p>
              <div className="s-header__contacts-line" />
              <p className="s-header__contacts-content">
                Партнерам: <a href="mailto:partner@eva-bio.ru">partner@eva-bio.ru</a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className={isActive ? "s-header__back isActive":"s-header__back"} onClick={showItems}/>
    </header>
    
  );
};

export default Header;