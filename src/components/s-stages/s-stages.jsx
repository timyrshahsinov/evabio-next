import Image from 'next/image'
import './s-stages.scss'

const stages = [
  {
    id:'01',
    title:'Цифровая трансформация',
    description:`активное использование самообучаемых нейросетей в диагностике и лечении пациентов`
  },
  {
    id:'02',
    title:'Информатизация',
    description:'автоматизированный сбор медицинских данных и их обработка в научно-исследовательских целях'
  },
  {
    id:'03',
    title:'Цифровизация',
    description:'автоматизация рутинных функций'
  },
]

const SStages = () => {
  return (
    <section className="s-stages">
      <div className="l-default s-stages__container">
        <div className="s-stages__title">
          <p className="s-stages__title-subtitle title--prefix">СПЕЦИАЛИСТЫ КОМПАНИИ ВЫДЕЛИЛИ</p>
          <h2 className="title--h2">3 этапа развития <span>медицинских ИТ</span></h2>
          <p className="s-stages__title-prefix title--prefix">и создания общей экосистемы EVA</p>
        </div>
        <div className="s-stages__content">
          <div className="s-stages__items">
            {stages.map(item=>{
              return(
                <div className="s-stages__item">
                  <div className="s-stages__item-container">
                    <div className="s-stages__item-number">
                      <Image
                        src={`/s-stages/${item.id}.svg`}
                        alt={item.title}
                        fill
                      />
                    </div>
                    <div className="s-stages__item-text">
                      <div className="s-stages__item-title">{item.title}</div>
                      <div className="s-stages__item-description">{item.description}</div>
                    </div>
                  </div>
                </div>
              )})}
          </div>
        </div>
      </div>
    </section>
  )}

export default SStages