import './s-using.scss'
import Image from 'next/image'

const usings = [
  {
    title: 'SIGNUM - V',
    text: `ПАК для диагностирования и определения факторов риска развития тромбоэмболии, путем круглосуточного дистанционного мониторинга измерения показателей ЭКГ и автоматического выявления изменений в деятельности сердца.`,
    img: `signum`
  },
  {
    title: 'EVA Oculus',
    text: `Комплекс видео и фото фиксации проводимых операций, с дистанционной поддержкой принятия решений.`,
    img: 'oculus'
  }
]

const SUsing = () => {
  return (
    <section className="s-using">
      <div className="l-default s-using__container">
        <div className="s-using__title">
          <p className="s-using__title-prefix title--prefix">Разрабатываемые <br/>программно-аппаратные комплексы</p>
          <h2 className='title--h2'>
            использующие <span>системы CDSS</span> 
          </h2>
        </div>
        <div className="s-using__content">
          <div className="s-using__items">
            {usings.map((item, index)=>{
              return (
                <div key={index} className="s-using__item">
                  <div className="s-using__item-text">
                    <h4 className="s-using__item-title">{item.title}</h4>
                    <p className="s-using__item-description">{item.text}</p>
                  </div>
                  <div className="s-using__item-img">
                    <Image
                      src={`/s-using/${item.img}.png`}
                      alt={item.img}
                      fill
                    />
                  </div>
                </div>
              )})}
          </div>
        </div>
      </div>
    </section>
  )
}

export default SUsing