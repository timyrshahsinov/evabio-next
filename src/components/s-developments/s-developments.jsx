"use client"

import { Swiper, SwiperSlide } from 'swiper/react';
import React, { useState, useEffect, useRef } from 'react';

import './s-developments.scss'
import 'swiper/css';

const developments = []

const SDevelopments = () => {
  return (
    <section className="s-developments">
      <div className="l-default s-developments__container">
        <div className="s-developments__title">
          <p className="s-developments__title-subtitle title--prefix">достижения нашей компании и</p>
          <h2 className='title--h2'>Зарегистрированные <span> разработки</span></h2>
        </div>
        <div className="s-developments__content">
          <div className="s-developments__swiper">
            <div className="s-developments__swiper-btn">
              <div className="s-developments__btn-left s-developments__btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" fill="none">
                  <path d="M18.9819 29.5121C18.5914 29.9027 18.5914 30.5358 18.9819 30.9263L25.3459 37.2903C25.7364 37.6808 26.3696 37.6808 26.7601 37.2903C27.1506 36.8998 27.1506 36.2666 26.7601 35.8761L21.1032 30.2192L26.7601 24.5624C27.1506 24.1719 27.1506 23.5387 26.7601 23.1482C26.3696 22.7576 25.7364 22.7576 25.3459 23.1482L18.9819 29.5121ZM40.7002 29.2192L19.689 29.2192L19.689 31.2192L40.7002 31.2192L40.7002 29.2192Z" />
                  <circle cx="29.8441" cy="29.8438" r="25" transform="rotate(105 29.8441 29.8438)" strokeWidth="2" />
                </svg>
              </div>
              <div className="s-developments__btn-rigth s-developments__btn">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" fill="none">
                  <path d="M41.0181 29.5121C41.4086 29.9027 41.4086 30.5358 41.0181 30.9263L34.6541 37.2903C34.2636 37.6808 33.6304 37.6808 33.2399 37.2903C32.8494 36.8998 32.8494 36.2666 33.2399 35.8761L38.8968 30.2192L33.2399 24.5624C32.8494 24.1719 32.8494 23.5387 33.2399 23.1482C33.6304 22.7576 34.2636 22.7576 34.6541 23.1482L41.0181 29.5121ZM19.2998 29.2192L40.311 29.2192L40.311 31.2192L19.2998 31.2192L19.2998 29.2192Z" />
                  <circle cx="26" cy="26" r="25" transform="matrix(0.258819 0.965926 0.965926 -0.258819 -1.6875 11.459)" strokeWidth="2"/>
                </svg>
              </div>
            </div>
            <div className="s-developments__swiper-container">
              <Swiper>
                {developments.map((item, imdex) =>{
                  return(
                    <SwiperSlide key={imdex} className='s-developments__swiper-slide'>
                    
                    </SwiperSlide>
                )})}
              </Swiper>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default SDevelopments