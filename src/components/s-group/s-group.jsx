"use client"

import Image from 'next/image';
import { useState, useEffect } from "react"
import './s-group.scss'

const logos = ['mtb', 'biomark', 'unitiss', 'legal']

const SGroup = () => {
    const [isLargeScreen, setIsLargeScreen] = useState(false);

  useEffect(() => {
    const checkScreenSize = () => {
      setIsLargeScreen(window.innerWidth > 992);
    };

    // Check on mount
    checkScreenSize();

    // Add event listener
    window.addEventListener('resize', checkScreenSize);

    // Clean up
    return () => window.removeEventListener('resize', checkScreenSize);
  }, []);

  const getLogoSrc = (logoName) => {
    return `/s-group/${logoName}${isLargeScreen ? '-992' : ''}.svg`;
  };
  return (
    <section className="s-group">
      <div className="l-default s-group__container">
        <h2 className="s-group__title title--h2">
          Mediss Group
        </h2>
        <div className="s-group__content">
          <div className="s-group__content-title">
            В составе группы компаний
          </div>
          <div className="s-group__content-logos">
            {logos.map((item, index) => {
              return (
                <div key={index} className="s-group__content-logo">
                  <div className="s-group__logo-container">
                    <Image
                      src={getLogoSrc(item)}
                      alt={item}
                      fill
                    />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </section>
  );
};

export default SGroup;