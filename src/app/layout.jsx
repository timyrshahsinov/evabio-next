
import '@/assets/styles/main.scss';
import AHeader from '@/components/s-header/s-header.jsx';
import AFooter from '@/components/s-footer/s-footer.jsx';
import { Manrope } from 'next/font/google'
const manrope = Manrope({ subsets: ['latin'] })

export const metadata = {
  title: 'Next.js',
  description: 'Generated by Next.js',
}

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={manrope.className}>
        <div id="root">
          <AHeader />
          <main>
            {children}
          </main>
          <AFooter /> 
          </div>
        </body>
    </html>
  )
}
