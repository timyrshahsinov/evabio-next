import SUsing from '@/components/s-using/s-using.jsx'
import SGroup from '@/components/s-group/s-group.jsx';
import SIntegration from '@/components/s-integration/s-integration.jsx';
import SSuccessful from '@/components/s-successful/s-successful';
import SStages from '@/components/s-stages/s-stages';
import SPurpose from '@/components/s-purpose/s-purpose';
import SDevelopments from '@/components/s-developments/s-developments';
import SMethods from '@/components/s-methods/s-methods'

export default function Home() {
  return (
    <div>
      <SPurpose />
      <SStages />
      <SMethods />
      <SSuccessful />
      <SUsing />
      <SIntegration />
      <SGroup />
      <SDevelopments />
    </div>
  );
}
